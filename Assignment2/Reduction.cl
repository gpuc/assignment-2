
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Reduction_InterleavedAddressing(__global uint* array, uint stride) 
{
	int i = 2 * stride * get_global_id(0);
	array[i] += array[i + stride];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Reduction_SequentialAddressing(__global uint* array, uint stride) 
{
	int i = get_global_id(0);
	array[i] += array[i + stride];
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Reduction_Decomp(const __global uint* inArray, __global uint* outArray, uint N, __local uint* localBlock)
{
	int gid = get_global_id(0);
	int lid = get_local_id(0);
	int ls = get_local_size(0);

	localBlock[lid] = inArray[gid] + inArray[gid + get_global_size(0)];
	barrier(CLK_LOCAL_MEM_FENCE);

	// local reduction
	for (int stride = 1; stride < ls; stride *= 2)
	{
		int i = 2 * lid * stride;
		if (i < ls)
		{
			localBlock[i] += localBlock[i + stride];
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	if (lid == 0)
	{
		outArray[get_group_id(0)] = localBlock[lid];
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void reduce(__local uint* localBlock, uint stride)
{
	int i = 2 * get_local_id(0) * stride;
	if (i < get_local_size(0))
	{
		localBlock[i] += localBlock[i + stride];
	}

	barrier(CLK_LOCAL_MEM_FENCE);
}

__kernel void Reduction_DecompUnroll(const __global uint* inArray, __global uint* outArray, uint N, __local uint* localBlock)
{
	int gid = get_global_id(0);
	int lid = get_local_id(0);
	int ls = get_local_size(0);

	localBlock[lid] = inArray[gid] + inArray[gid + get_global_size(0)];
	barrier(CLK_LOCAL_MEM_FENCE);

	// local reduction
	if (ls > 1) reduce(localBlock, 1);
	if (ls > 2) reduce(localBlock, 2);
	if (ls > 4) reduce(localBlock, 4);
	if (ls > 8) reduce(localBlock, 8);
	if (ls > 16) reduce(localBlock, 16);
	if (ls > 32) reduce(localBlock, 32);
	if (ls > 64) reduce(localBlock, 64);
	if (ls > 128) reduce(localBlock, 128);
	if (ls > 256) reduce(localBlock, 256);

	for (int stride = 512; stride < ls; stride *= 2)
	{
		reduce(localBlock, stride);
	}

	if (lid == 0)
	{
		outArray[get_group_id(0)] = localBlock[lid];
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Reduction_DecompAtomics(const __global uint* inArray, __global uint* outArray, uint N, __local uint* localSum)
{
	int gid = get_global_id(0);
	int lid = get_local_id(0);

	localSum[lid] = inArray[gid] + inArray[gid + get_global_size(0)];
	barrier(CLK_LOCAL_MEM_FENCE);

	// local reduction
	if (lid > 0)
	{
		atomic_add(localSum, localSum[lid]);
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	if (lid == 0)
	{
		outArray[get_group_id(0)] = localSum[lid];
	}
}
