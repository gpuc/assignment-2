


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Scan_Naive(const __global uint* inArray, __global uint* outArray, uint N, uint offset) 
{
	int i = get_global_id(0);
	if (i < offset)
	{
		outArray[i] = inArray[i];
	}
	else
	{
		outArray[i] = inArray[i] + inArray[i - offset];
	}
}



// Why did we not have conflicts in the Reduction? Because of the sequential addressing (here we use interleaved => we have conflicts).

#define UNROLL
#define NUM_BANKS			32
#define NUM_BANKS_LOG		5
#define SIMD_GROUP_SIZE		32

// Bank conflicts
#define AVOID_BANK_CONFLICTS
#ifdef AVOID_BANK_CONFLICTS
	#define OFFSET(A) ((A) + (A) / NUM_BANKS)
#else
	#define OFFSET(A) (A)
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Scan_WorkEfficient(__global uint* array, __global uint* higherLevelArray, __local uint* localBlock) 
{
	int gid = get_global_id(0);
	int lid = get_local_id(0);
	int ls = get_local_size(0);
	int globalArrayIndex = 2 * gid;
	int localBlockIndex = 2 * lid;
	int localBlockSize = 2 * ls;
	int lastBlockIndex = localBlockSize - 1;

	// read input
	localBlock[OFFSET(localBlockIndex)] = array[globalArrayIndex];
	localBlock[OFFSET(localBlockIndex + 1)] = array[globalArrayIndex + 1];
	barrier(CLK_LOCAL_MEM_FENCE);

	// up-sweep
	int stride;
	for (stride = 1; stride < localBlockSize; stride *= 2)
	{
		int i = lastBlockIndex - localBlockIndex * stride;
		if (i - stride >= 0 && i < localBlockSize)
		{
			localBlock[OFFSET(i)] += localBlock[OFFSET(i - stride)];
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// set last element to 0
	if (localBlockIndex + 1 == lastBlockIndex)
	{
		localBlock[OFFSET(lastBlockIndex)] = 0;
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	// down-sweep
	for (stride /= 2; stride > 0; stride /= 2)
	{
		int i = lastBlockIndex - localBlockIndex * stride;
		if (i - stride >= 0 && i < localBlockSize)
		{
			int old = localBlock[OFFSET(i)];
			localBlock[OFFSET(i)] += localBlock[OFFSET(i - stride)];
			localBlock[OFFSET(i - stride)] = old;
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// write back results
	array[globalArrayIndex] += localBlock[OFFSET(localBlockIndex)];
	array[globalArrayIndex + 1] += localBlock[OFFSET(localBlockIndex + 1)];

	barrier(CLK_GLOBAL_MEM_FENCE);
	if (localBlockIndex + 1 == lastBlockIndex)
	{
		higherLevelArray[get_group_id(0)] = array[globalArrayIndex + 1];
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__kernel void Scan_WorkEfficientAdd(__global uint* higherLevelArray, __global uint* array, __local uint* localBlock) 
{
	if (get_group_id(0) == 0)
		return;

	int gid = get_global_id(0);
	int globalArrayIndex = 2 * gid;

	// add value from higher level array to every local entry of the result array
	uint value = higherLevelArray[get_group_id(0) - 1];
	array[globalArrayIndex] += value;
	array[globalArrayIndex + 1] += value;
}
